#!/usr/bin/env perl

use Path::Tiny;
use WWW::Mechanize;
use File::chdir;
use autodie;
use IPC::System::Simple;

sub main {
	my $conformer = path('conformer-setup')->absolute;
	$conformer->mkdir;

	my $fileid = '1p46Ny6IVVdEmXUYhtj_X_EGgJIlAfh3C';
	my $filename = $conformer->child('pretrained-subword-conformer.zip');

	unless( -f $filename ) {
		my $mech = WWW::Mechanize->new( autocheck => 1 );
		$mech->get("https://drive.google.com/uc?export=download&id=${fileid}");
		$mech->click;
		$filename->spew_raw( $mech->response->decoded_content );
	}

	my $model_dir = path( $filename =~ s/\.zip\z//r );
	unless( -d $model_dir ) {
		system( 'unzip',
			qw(-d),  $model_dir,
			$filename,
		);
	}

	my $upstream_project = $conformer->child('TensorFlowASR');
	unless( -d $upstream_project ) {
		system(
			qw(git clone),
				qw(-b v1.0.3),
				'https://github.com/TensorSpeech/TensorFlowASR.git',
				$upstream_project,
		);
	}

	my $venv = $conformer->child('venv');
	{
		local $CWD = "$conformer";
		local $ENV{VENV_DIR} = "$venv";
		unless( -d $venv ) {
			system( qw( python3 -m venv), $venv );
			system( qw(bash -c), <<~'EOF' );
			. $VENV_DIR/bin/activate;
			( cd TensorFlowASR && pip3 install -e ".[tf2.8]" )
			pip3 install 'numpy<1.24'
			pip3 install 'tensorflow_io'
			EOF
		}

		$model_dir->child('config.yml')->edit_utf8(sub {
			s,/mnt/,${conformer}/work/,g;
		});

		system( qw(bash -c), <<~'EOF' ) if 0;
		. $VENV_DIR/bin/activate;
		python3 ./TensorFlowASR/examples/demonstration/conformer.py             \
			--config pretrained-subword-conformer/config.yml                \
			--saved pretrained-subword-conformer/latest.h5                  \
			--subwords pretrained-subword-conformer/conformer.subwords      \
			TensorFlowASR/examples/demonstration/wavs/2033-164915-0001.flac
		EOF
	}

	{
		local $ENV{VENV_DIR} = "$venv";
		system( qw(bash -c), <<~'EOF' );
		. $VENV_DIR/bin/activate;
		python3 convert-model.py             \
			--config conformer-setup/pretrained-subword-conformer/config.yml                \
			--saved conformer-setup/pretrained-subword-conformer/latest.h5                  \
			--subwords conformer-setup/pretrained-subword-conformer/conformer.subwords      \
			conformer-setup/TensorFlowASR/examples/demonstration/wavs/2033-164915-0001.flac
		EOF
	}

}

main unless caller;

__END__
