package TF_ASR::SpeechFeaturizer;
# ABSTRACT: Speech featurizer

package Base {
	# See SpeechFeaturizer
	use Mu;
	use Function::Parameters;

	# Samples
	ro 'sample_rate' => ( default => 16000 );
	ro 'frame_ms' => ( default => 25 );
	lazy frame_length => method() {
		int($self->sample_rate * ($self->frame_ms / 1000));
	};
	lazy stride_ms => ( default => 10 );

	lazy frame_step => method() {
		int($self->sample_rate * ($self->stride_ms / 1000));
	};

        # Features
	ro num_feature_bins => ( default =>  80 );
        ro feature_type => ( default => "log_mel_spectrogram" );
        ro preemphasis => ( default => undef );
        ro top_db => ( default => 80.0 );

        # Normalization
        ro normalize_signal => ( default => true );
        ro normalize_feature => ( default => true );
        ro normalize_per_frame => ( default => false );
        ro center => ( default => true );
        # Length
	ro max_length => ( default => 0 );
}

package PDL_Feature {
	method stft( $signal ) {
		...
	}

	method compute_log_mel_spectrogram( $signal ) {
		...
	}

}

1;
